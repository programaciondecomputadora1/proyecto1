#include <stdio.h> 
#include<math.h> /* esta libreria nos ayuda para resolver raiz cuadradas, entre otras operaciones..*/
#include<conio.h>

int main() /*Inicio del programa: Elaborar una aplicaci�n de l�nea de comandos en C que calcule la expresi�n de segundo grado:

ax^2+bx+c=0*/
{
	float a, b, c, d, X1=0,X2=0; /*declaramos las variables que vamos a utilizar para asignarle los valores de la ecuacion y asignar resultados*/
	printf ("\nEcuacion de segundo grado\n\n"); /* Mensaje del problema*/
	printf ("  2\n");
	printf ("ax + bx + c = 0\n\n");
	printf ("Ingresa el valor de a: ");
	scanf ("%f", &a);
	printf ("Ingresa el valor de b: ");
	scanf ("%f", &b);
	printf ("Ingresa el valor de c: ");
	scanf ("%f", &c);
	
	if (a!=0)/*valos es diferete de 0, realiza el calculo*/
	{ 
	
		if (b*b-4*a*c>=0)/* si el discriminante es mayor que 0 hace el calculo de las raices*/
		{
		X1=(-b+sqrt(b*b-4*a*c))/(2*a);
		X2=(-b-sqrt(b*b-4*a*c))/(2*a);
		printf ("\t\El resultado de x1 es: %f",X1);
		printf ("\t\El resultado de x2 es: %f",X2);
		}
		else /* si el discrminante es menor que 0, las raices son complejas e imprime el mensaje*/
		printf ("Las raices son complejas");
	}
	else
	printf ("Esta escuacion solo tiene una raiz: %f",(-c/b)); /* si el coeficiente a=0, la escuacion solo tiene una raiz (-c/b)*/
	return 0;
	
	
}
